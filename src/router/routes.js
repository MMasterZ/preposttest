const routes = [{
    path: '/',
    component: () => import('pages/login.vue'),
    name: "login"
  }, {
    path: '/welcome',
    component: () => import('pages/welcome.vue'),
    name: "welcome"
  }, {
    path: '/start',
    component: () => import('pages/start.vue'),
    name: "start"
  }, {
    path: '/finish',
    component: () => import('pages/finish.vue'),
    name: "finish"
  },
  {
    path: '/test',
    component: () => import('layouts/MyLayout.vue'),
    children: [{
        path: '',
        component: () => import('pages/Index.vue')
      },

    ]
  },
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
