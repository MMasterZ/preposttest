const state = {
  teststudentData: "",
  testexamprepostData: "",
  testlevelexam: "",
};

const mutations = {
  setUserData: (state, payload) => {
    //Set ค่า userData
    state.teststudentData = payload;
  },
  setExamData: (state, payload) => {
    //Set ค่า Pre Post Data
    state.testexamprepostData = payload;
  },
  setLevelData: (state, payload) => {
    //Set ค่า Pre Post Data
    state.testlevelexam = payload;
  }
};

const actions = {
  setUserData: ({
    commit
  }, payload) => {
    commit("setUserData", payload);
  },
  setExamData: ({
    commit
  }, payload) => {
    commit("setExamData", payload);
  },
  setLevelData: ({
    commit
  }, payload) => {
    commit("setLevelData", payload);
  }
};

export default {
  state,
  mutations,
  actions
};
